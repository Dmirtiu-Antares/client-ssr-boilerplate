# Blockchain client side

#stuff todo
- tests -> need to research https://github.com/zapkub/mocha-test-typescript
- dist chunks in a some folder

#Readme:
This app is not about interface beauty, but about architecture. Researching of new solutions is in progress, so template is not done yet.

What you will see:
- gitlab CI + AWS EB automatization deploy process
- SSR in a beautiful helpers form 
- chunks system (lazy loading for page, that client do not need in the current moment in browser, also thay will be cashed for next fast load )
- awesome CSS modules and POST CSS that renders on a server (it makes our loading little bit faster)
- Typescript (but for now there are not a lot of interface and a lot of any, sorry about that, need time to thinks about better type structure for this architecture)
- There is not TSLINT and autofix on this branch for now, so code style is not that great. It`s in process
- A few options about "how to render component for mobile/desktop" - check out Blockchains and Blockchain (also here is a lazy load, so on mobile we do not download desktop version!)
- Saga helper for TS, that makes work with redux easier and faster
- some custom webpack modules
- React router 4



address: http://localhost:3030/

#Development
Frontend:
```sh
yarn
yarn build-dev (only for a first time)
yarn dev
```


#Production
Frontend:
```sh
yarn 
yarn build-prod
yarn start
```

Notes for me:

commands to deploy new project are basicly here: 
https://medium.com/@xoor/deploying-a-node-js-app-to-aws-elastic-beanstalk-681fa88bac53