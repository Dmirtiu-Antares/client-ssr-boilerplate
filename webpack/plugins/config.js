
const environment = {
    development: {
        apiPath: `http://localhost:${process.env.PORT}/api/`,
        isClient: typeof(window) !== "undefined"
    },
    production: {
        apiPath: `http://client--ssr-boilerplate-dev.us-west-2.elasticbeanstalk.com/api/`,
        isClient: typeof(window) !== "undefined"
    }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({}, environment);